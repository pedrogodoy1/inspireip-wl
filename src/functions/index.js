import axios from 'axios'

const f = () => {
  const axiosToken = () => {
    let headers = { 'Content-Type': 'application/json' }
    const tokenUser = window.localStorage.getItem('inspireip_token')
    let token = ''
    if (tokenUser) {
      token = tokenUser
      headers = { 'x-access-token': token, 'Content-Type': 'application/json' }
    }
    axios.create({
      baseURL: process.env.API,
      headers: headers
    })
    axios.defaults.baseURL = process.env.API
    axios.defaults.headers.common['x-access-token'] = token
    axios.defaults.headers.post['Content-Type'] = 'application/json'

    return axios
  }

  const axiosTokenAdmin = () => {
    let headers = { 'Content-Type': 'application/json' }
    const tokenUserAdmin = window.localStorage.getItem('inspireip_token_admin')
    let token = ''
    if (tokenUserAdmin) {
      token = tokenUserAdmin
      headers = { 'x-access-token': token, 'Content-Type': 'application/json' }
    }
    axios.create({
      baseURL: process.env.API,
      headers: headers
    })
    axios.defaults.baseURL = process.env.API
    axios.defaults.headers.common['x-access-token'] = token
    axios.defaults.headers.post['Content-Type'] = 'application/json'

    return axios
  }

  const conversaoDolar = async () => {
    let resultado
    let error
    await axios.get('https://economia.awesomeapi.com.br/last/USD').then(res => {
      resultado = res.data.USDBRL.bid
      error = null
    }).catch(e => {
      error = e
      resultado = null
    })
    return { resultado, error }
  }

  const conversaoEth = async () => {
    let resultado
    let error
    await axios.get('https://www.mercadobitcoin.net/api/ETH/ticker/').then(res => {
      resultado = res.data.ticker.last
      error = null
    }).catch(e => {
      error = e
      resultado = null
    })
    return { resultado, error }
  }

  return { axiosToken, conversaoDolar, conversaoEth, axiosTokenAdmin }
}

export default f
