import axios from 'axios'
import f from '../../functions/index'

export function getArtist ({ commit }, { userId }) {
  axios.get('/users/artist/' + userId).then(res => {
    commit('SET_USER', res.data.artist)
  })
}

export function getUser ({ commit }) {
  f().axiosToken().get('/users').then(res => {
    commit('SET_USER', res.data)
  })
}

export function addUser ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    // CAPTURA DA STRING DE DATA O DIA, MES E ANO
    const dia = dados.dataNascimento.substr(0, 2)
    const mes = dados.dataNascimento.substr(3, 2)
    const ano = dados.dataNascimento.substr(6, 9)

    // UTILIZA COMO BASE PARA TRANSFORMAR A STRING EM DATA
    const data = new Date(mes + '/' + dia + '/' + ano + ' ' + '00:00')

    // CAPTURA O HORARIO LOCAL E RETIRA A DIFERENÇA UTC (CAPTURANDO O HORARIO CORRETO DO USUÁRIO)
    const dataFormatada = data.getTime() - (data.getTimezoneOffset() * 60000)

    // CONVERTE A DATA PARA O PADRAO ISO 8601 (PADRÃO DA API)
    // .split DIVIDE A DATA NO INICIO DO HORARIO E O 0 CAPTURA APENAS A DATA SEM HORARIO
    const dataGlobalIso = new Date(dataFormatada).toISOString().split('T')[0]

    const dataString = dataGlobalIso.toString()

    const formData = new FormData()
    formData.append('name', dados.nome)
    formData.append('lastname', dados.sobrenome)
    formData.append('email', dados.email)
    formData.append('birth_date', dataString)
    formData.append('password', dados.confirmaSenha)
    formData.append('file', dados.imagem[0])

    axios.post('/users', formData).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updateUser ({ commit, dispatch }, { dados, dadosImg, id }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      artist_name: dados.nome_artista,
      artist_type: dados.tipo_artista,
      artist_description: dados.descricao_artista,
      artist_twitter: dados.twitter_artista,
      artist_facebook: dados.facebook_artista,
      artist_instagram: dados.instagram_artista,
      artist_twitch: dados.twitch_artista,
      artist_youtube: dados.youtube_artista
    }

    f().axiosToken().post('/users/update-artist', newDados).then(async res => {
      commit('SET_USER', newDados)

      if (dadosImg[0]) {
        const formData = new FormData()
        formData.append('file', dadosImg[0])

        await f().axiosToken().post('/users/update-profile-picture', formData)

        dispatch('getArtist', { userId: id })
        resolve(res)
      } else {
        resolve(res)
      }

      /* if (dados.carteira_artista) {
        const walletDados = {
          ethereum_wallet: dados.carteira_artista.toLowerCase()
        }

        await f().axiosToken().post('/users/update-wallet', walletDados)
        commit('SET_USER', walletDados)
        resolve(res)
      } else {
        resolve(res)
      } */
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updateUserProfilePicture ({ dispatch }, { dadosImg }) {
  return new Promise((resolve, reject) => {
    if (dadosImg.imagem) {
      const formData = new FormData()
      formData.append('file', dadosImg.imagem)

      f().axiosToken().post('/users/update-profile-picture', formData).then(res => {
        dispatch('getUser')
        resolve(res)
      }).catch(error => {
        reject(error.response)
      })
    }
  })
}

export function updateWallet ({ commit, dispatch }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosToken()
      .post('/users/update-wallet', { ethereum_wallet: dados.carteira_artista })
      .then(async res => {
        commit('SET_USER', { ethereum_wallet: dados.carteira_artista })
        resolve(res)
      })
      .catch(error => {
        console.log(error)
        reject(error.response)
      })
  })
}

export function updateUserBanner ({ dispatch }, { dadosImg, id }) {
  return new Promise((resolve, reject) => {
    const formData = new FormData()

    formData.append('file', dadosImg[0])

    f().axiosToken().post('/users/update-artist-banner', formData).then(res => {
      dispatch('getArtist', { userId: id })
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function uploadMedia ({ commit }, { dadosImg, dadosImgs }) {
  return new Promise((resolve, reject) => {
    if (dadosImg) {
      const formData = new FormData()

      formData.append('file', dadosImg)

      f().axiosToken().post('/media', formData).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error.response)
      })
    } else {
      for (let i = 0; i < dadosImgs.length; i++) {
        const formData = new FormData()

        formData.append('file', dadosImgs[i].img)
        f().axiosToken().post('/media', formData).then(res => {
          resolve(res)
        }).catch(error => {
          reject(error.response)
        })
      }
    }
  })
}

export function loginUser ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    axios.post('/users/authenticate', dados).then(res => {
      window.localStorage.setItem('inspireip_token', res.data.token)

      commit('SET_TOKEN', res.data.token || '')
      commit('SET_USER', res.data.user || {})
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function loginUserAdmin ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    axios
      .post('/admin/authenticate', dados)
      .then(res => {
        window.localStorage.setItem('inspireip_token_admin', res.data.token)

        commit('SET_TOKEN_ADMIN', res.data.token || '')
        commit('SET_USER_ADMIN', res.data.admin || {})
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function loginUser2FA ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    axios
      .post('/users/authenticate/validate', dados)
      .then(res => {
        window.localStorage.setItem('inspireip_token', res.data.token)

        commit('SET_TOKEN', res.data.token || '')
        commit('SET_USER', res.data.user || {})
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function logoutUsers ({ commit }) {
  commit('LOGGOUT')
  window.localStorage.removeItem('inspireip_token')
}

export function forgotPassword ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      email: dados.email
    }

    axios.post('/users/forgot-password', newDados).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updatePassword ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      recovery_code: dados.codigo,
      password: dados.confirmarNovaSenha
    }

    axios.post('/users/update-password', newDados).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updatePasswordLogged ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      password: dados.confirmarNovaSenha
    }

    f().axiosToken().post('/users/update-password-logged', newDados).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function forgotPasswordAdmin ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      email: dados.email
    }

    axios.post('/admin/forgot-password', newDados).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updatePasswordAdmin ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      recovery_code: dados.codigo,
      password: dados.confirmarNovaSenha
    }

    axios.post('/admin/update-password', newDados).then(res => {
      resolve(res)
    }).catch(error => {
      reject(error.response)
    })
  })
}

export function updatePasswordAdminLogged ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    const newDados = {
      password: dados.confirmarNovaSenha
    }

    f()
      .axiosTokenAdmin()
      .post('admin/update-password-logged', newDados)
      .then(res => {
        resolve(res)
      }).catch(error => {
        reject(error.response)
      })
  })
}

export function updateUserAdmin ({ commit, dispatch }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/update', { ...dados })
      .then(async res => {
        commit('SET_USER_ADMIN', { ...dados })
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function listAdmin ({ commit, dispatch }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .get('/admin/list')
      .then(async res => {
        commit('SET_USER_LIST_ADMIN', [...res.data])
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function listUsers ({ commit, dispatch }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .get('/admin/list/users')
      .then(async res => {
        commit('SET_USER_LIST_USERS', [...res.data])
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function addUserAdmin ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin', dados)
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function activeAdmin ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/activate', { id: id })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function inactivateAdmin ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/inactivate', { id: id })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function deleteAdmin ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/delete', { id: id })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function changeUserType ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/update-user', { ...dados })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function logoutAdmin ({ commit }) {
  commit('LOGGOUT_ADMIN')
  window.localStorage.removeItem('inspireip_token')
}

export function deleteUser ({ commit }, { id }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/user/delete', { id: id })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function listBanners ({ commit }) {
  return new Promise((resolve, reject) => {
    axios.get('users/banners')
      .then(res => {
        resolve(res)

        commit('SET_USER_LIST_BANNERS', [...res.data])
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function updateUserArtist ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('/admin/update-user', { ...dados })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function listArtist ({ commit, dispatch }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .get('/admin/list/users')
      .then(async res => {
        const artists = res.data.filter(i => i.artist)
        commit('SET_USER_LIST_ARTIST', [...artists])
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function deleteWallet ({ commit }, { dados }) {
  return new Promise((resolve, reject) => {
    f()
      .axiosTokenAdmin()
      .post('admin/update-wallet', { ...dados })
      .then(res => {
        resolve(res)
      })
      .catch(error => {
        reject(error.response)
      })
  })
}

export function getPublicArtistById ({ commit }, { userId }) {
  axios.get('/users/artist/' + userId).then(res => {
    commit('SET_PUBLIC_ARTIST', res.data.artist)
  })
}

export function clearPublicArtist ({ commit }) {
  commit('SET_PUBLIC_ARTIST', {})
}
