export function getUser (state) {
  return state.user
}

export function getToken (state) {
  return state.token
}

export function getUserAdmin (state) {
  return state.admin
}

export function getListAdmin (state) {
  return state.list_admin
}

export function getListUsers (state) {
  return state.list_users
}

export function getTokenAdmin (state) {
  return state.token_admin
}

export function getListBanners (state) {
  return state.list_banners
}

export function getListArtist (state) {
  return state.list_artist
}

export function getPublicArtist (state) {
  return state.public_artist
}
