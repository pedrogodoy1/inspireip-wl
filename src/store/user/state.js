export default function () {
  return {
    user: {},
    admin: {},
    public_artist: {},
    list_admin: [],
    list_users: [],
    list_artist: [],
    list_banners: [],
    token_admin: '',
    token: ''
  }
}
