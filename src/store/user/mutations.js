export const SET_USER = (state, val) => {
  state.user = { ...state.user, ...val }
}
export const SET_TOKEN = (state, val) => {
  state.token = val
}
export const LOGGOUT = (state) => {
  state.user = {}
  state.token = ''
}

export const SET_USER_ADMIN = (state, val) => {
  state.admin = val
}
export const SET_TOKEN_ADMIN = (state, val) => {
  state.token_admin = val
}
export const LOGGOUT_ADMIN = state => {
  state.admin = {}
  state.token_admin = ''
}

export const SET_USER_LIST_ADMIN = (state, val) => {
  state.list_admin = val
}

export const SET_USER_LIST_USERS = (state, val) => {
  state.list_users = val
}

export const SET_USER_LIST_BANNERS = (state, val) => {
  state.list_banners = val
}

export const SET_USER_LIST_ARTIST = (state, val) => {
  state.list_artist = val
}

export const SET_PUBLIC_ARTIST = (state, val) => {
  state.public_artist = val
}
