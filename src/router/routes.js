const routes = [
  // {
  //   path: '/',
  //   component: () => import('src/layouts/LayoutCabecalhoLP.vue'),
  //   children: [
  //     {
  //       path: '',
  //       name: 'lp',
  //       component: () => import('src/pages/LandingPage.vue')
  //     }
  //   ]
  // },
  // ------------------------ ROTA DE LAYOUT AZUL ------------------------
  {
    path: '/',
    component: () => import('src/layouts/LayoutCabecalho.vue'),
    children: [
      {
        path: '',
        name: 'lp',
        component: () => import('src/pages/LandingPage.vue')
      },
      {
        path: '/perfilpessoal',
        name: 'perfilpessoal',
        component: () => import('src/pages/PerfilPessoal.vue')
      },
      {
        path: '/perfilcolecionador/:slug',
        name: 'perfilcolecionador',
        component: () => import('src/pages/PerfilColecionador.vue')
      },
      // ------------------------ Alterações ------------------------
      {
        path: '/primeirosdetalhes',
        name: 'primeirosdetalhes',
        component: () => import('src/pages/Cadastro/PrimeirosDetalhes.vue')
      },
      {
        path: '/compradireta',
        name: 'compradireta',
        component: () => import('src/pages/Compra/CompraDireta.vue')
      },
      {
        path: '/compraconfirmada',
        name: 'compraconfirmada',
        component: () => import('src/pages/Compra/CompraConfirmada.vue')
      },
      {
        path: '/fimleilao',
        name: 'fimleilao',
        component: () => import('src/pages/FimLeilao.vue')
      },
      {
        path: '/navegar',
        name: 'navegar',
        component: () => import('src/pages/Navegar.vue'),
        props: true
      },
      {
        path: '/pesquisa',
        name: 'pesquisa',
        component: () => import('src/pages/Pesquisa.vue')
      },
      {
        path: '/lance',
        name: 'lance',
        component: () => import('src/pages/Lance.vue')
      },
      {
        path: '/lanceregistrado',
        name: 'lanceregistrado',
        component: () => import('src/pages/LanceRegistrado.vue')
      },
      {
        path: '/lancecoberto',
        name: 'lancecoberto',
        component: () => import('src/pages/LanceCoberto.vue')
      },
      {
        path: '/perfilartista/:slug',
        name: 'perfilartista',
        component: () => import('src/pages/PerfilArtista.vue')
      }
    ]
  },

  // ------------------------ ROTA DE LAYOUT CLEAN ------------------------
  {
    path: '/',
    component: () => import('src/layouts/LayoutClean.vue'),
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import('src/pages/Login.vue')
      },
      {
        path: '/loginadmin',
        name: 'loginadmin',
        component: () => import('src/pages/Admin/LoginAdmin.vue')
      },
      {
        path: '/cadastroperfil',
        name: 'cadastroperfil',
        component: () => import('src/pages/Cadastro/CadastroPerfil.vue')
      },
      {
        path: '/recuperarsenha',
        name: 'recuperarsenha',
        component: () => import('src/pages/Cadastro/RecuperarSenha.vue')
      },
      {
        path: '/codigorecuperar',
        name: 'codigorecuperar',
        component: () => import('src/pages/Cadastro/CodigoRecuperarSenha.vue')
      },
      {
        path: '/alterarsenha',
        name: 'alterarsenha',
        component: () => import('src/pages/Cadastro/AlterarSenha.vue')
      },
      {
        path: '/carteiradigital',
        name: 'carteiradigital',
        component: () => import('src/pages/CarteiraDigital.vue')
      },
      {
        path: '/carteiradigitalexplicativo',
        name: 'carteiradigitalexplicativo',
        component: () => import('src/pages/CarteiraDigitalExplicativo.vue')
      },
      {
        path: 'recuperarsenhaadmin',
        name: 'recuperarsenhaadmin',
        component: () => import('src/pages/Admin/RecuperarSenhaAdmin.vue')
      },
      {
        path: 'codigorecuperaradmin',
        name: 'codigorecuperaradmin',
        component: () => import('src/pages/Admin/CodigoRecuperarSenhaAdmin.vue')
      },
      {
        path: 'termosuso',
        name: 'termosuso',
        component: () => import('src/pages/TermosUso.vue')
      },
      {
        path: 'politicaprivacidade',
        name: 'politicaprivacidade',
        component: () => import('src/pages/PoliticaPrivacidade.vue')
      }
    ]
  },
  // ------------------------ ROTA DE LAYOUT DASHBOARD ------------------------
  {
    path: '/',
    component: () => import('src/layouts/LayoutDash.vue'),
    children: [
      {
        path: 'paineladmin',
        name: 'paineladmin',
        component: () => import('src/pages/Admin/PainelAdmin.vue')
      },
      {
        path: 'usuariosadmin',
        name: 'usuariosadmin',
        component: () => import('src/pages/Admin/UsuariosAdmin.vue')
      },
      {
        path: 'usuarios',
        name: 'usuarios',
        component: () => import('src/pages/Admin/Usuarios.vue')
      },
      {
        path: 'siteconfig',
        name: 'siteconfig',
        component: () => import('src/pages/Admin/SiteConfig.vue')
      }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
