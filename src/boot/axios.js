import Vue from 'vue'
import axios from 'axios'

if (window.location.hostname.indexOf('inspireip') !== -1 || window.location.hostname.indexOf('nft') !== -1 || window.location.hostname.indexOf('localhost') !== -1) {
  axios.create({
    baseURL: process.env.API
  })
  axios.defaults.baseURL = process.env.API
  axios.defaults.headers.post['Content-Type'] = 'application/json'
}

Vue.prototype.$axios = axios
